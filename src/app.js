import "./component/app-bar.js";
import "./component/global/global-data"
import "./component/local/local-data.js";
import "bootstrap/dist/css/bootstrap.min.css";
import directapi from "./component/api/directapi";
import directlocal from "./component/api/directlocal";
import directtable from "./component/api/directtable";
import table from "./component/table";


directtable();
directapi();
directlocal();


