class GlobalData extends HTMLElement {

  connectedCallback() {
    this.render();
  }

  render() {
    this.innerHTML = `

    <p class="card-text"><small class="text-muted" id="lastupdate"></small></p>
    <div class="card-header">
    <h2>Global Live Suspect</h2>
</div>
    
    <div class="card-group ">
  <div class="card card text-white bg-light mb-3 border-0 rounded">
    <div class="card-body">
      <svg xmlns="http://www.w3.org/2000/svg" width="70" height="70" fill="#FFC009" class="bi bi-check" viewBox="0 0 16 16">
        <path fill-rule="evenodd" d="M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.236.236 0 0 1 .02-.022z"/>
      </svg>
      <h2 class="card-title text-warning"><b>Global Suspect</b></h2>
      <h1 class="card-title text-warning" id="confirmed"></h1>
          <h5 class="card-text text-dark">The number of suspect coronaviruses around the world</h5>
    </div>
  </div>
  <div class="card card text-white bg-light mb-3 border-0">
    <div class="card-body">
      <svg xmlns="http://www.w3.org/2000/svg" width="70" height="70" fill="#DC3545" class="bi bi-arrow-down-short" viewBox="0 0 16 16">
        <path fill-rule="evenodd" d="M8 4a.5.5 0 0 1 .5.5v5.793l2.146-2.147a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 1 1 .708-.708L7.5 10.293V4.5A.5.5 0 0 1 8 4z"/>
      </svg>
      <h2 class="card-title text-danger"><b>Global Deaths</b></h2>
      <h1 class="card-title text-danger" id="deaths">10.189.988</h1>  
       <h5 class="card-text text-dark">The confirmed death effect of the coronavirus</h5>
    </div>
  </div>
  <div class="card card text-white bg-light mb-3 border-0 roun">
    <div class="card-body">
    <svg xmlns="http://www.w3.org/2000/svg" width="70" height="70" fill="#198754" class="bi bi-arrow-up-short" viewBox="0 0 16 16">
    <path fill-rule="evenodd" d="M8 12a.5.5 0 0 0 .5-.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 .5.5z"/>
    </svg>
      <h2 class="card-title text-success"><b>Global Recovered</b></h2>
      <h1 class="card-title text-success" id="recovered"></h1>
            <h5 class="card-text text-dark">Recovering from the coronavirus</h5>
    </div>
  </div>
</div>
    `;
  }
}

customElements.define("global-data", GlobalData);
