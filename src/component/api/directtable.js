const apiDaily = "https://covid19.mathdro.id/api/daily/";


var currentdate = new Date(); 
var datetime = currentdate.getMonth()+1 + "-"
                + (currentdate.getDate()-2)  + "-" 
                + currentdate.getFullYear();    



const getDaily = () =>{
    const xhr= new XMLHttpRequest();
    xhr.onload = function ()  {
        const responseJson =  JSON.parse(this.responseText);
        if (responseJson.error) {
            showMessage(responseJson.message);
        } else {
            dailyData(responseJson);
            console.log(responseJson);
        }
    };
    xhr.onerror = () => {
        showMessage();
    };
    xhr.open("GET", `${apiDaily}${datetime}`);
    xhr.send();
};

 
                
const showMessage = (message = "Check your connection") => {
    alert(message);
}

const dailyData = (datas) => {
    const table = document
    .getElementById("table")
    .getElementsByTagName("tbody")[0];
    let no = 1;
    datas.forEach((data) => {
        table.insertRow().innerHTML = ` 
        <tr data-index="0">
         <td>${no++}.</td>
        <td>${data.countryRegion}</td>
        <td>${data.provinceState}</td>
        <td>${data.confirmed}</td>
        <td>${data.deaths}</td>
        <td>${data.recovered}</td>
        <td>${data.lastUpdate}</td>
        </tr>
        `;
    });
};

export default getDaily;