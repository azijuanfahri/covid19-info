import $ from "jquery";

const apiLocal = "https://covid19.mathdro.id/api/countries/id/";

const getData = () => {
  const xhr = new XMLHttpRequest();
  xhr.onload = function () {
    const responseJson = JSON.parse(this.responseText);
    if (responseJson.error) {
      console.log(responseJson.error);
      showResponseMessage(responseJson.message);
    } else {
      local(responseJson)
    }
  };

xhr.onerror = () => {
  showResponseMessage
};

xhr.open("GET", `${apiLocal}`);
xhr.send();
};

const showResponseMessage = (message = "Check your connection") => {
  alert(message);
};

const local = (data) => {
  $("#confirmed_l").text(data.confirmed.value);
  $("#deaths_l").text(data.deaths.value);
  $("#recovered_l").text(data.recovered.value);
  $("#lastupdate_l").text(data.lastUpdate);
};

export default getData;