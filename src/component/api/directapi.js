import $ from "jquery";

const apiGlobal = "https://covid19.mathdro.id/api/";

const getData = () => {
  const xhr = new XMLHttpRequest();
  xhr.onload = function () {
    const responseJson = JSON.parse(this.responseText);
    if (responseJson.error) {
      console.log(responseJson.error);
      showResponseMessage(responseJson.message);
    } else {
      global(responseJson)
    }
  };

xhr.onerror = () => {
  showResponseMessage
};

xhr.open("GET", `${apiGlobal}`);
xhr.send();
};

const showResponseMessage = (message = "Check your connection") => {
  alert(message);
};

const global = (data) => {
  $("#confirmed").text(data.confirmed.value);
  $("#deaths").text(data.deaths.value);
  $("#recovered").text(data.recovered.value);
  $("#lastupdate").text(data.lastUpdate);
};

export default getData;